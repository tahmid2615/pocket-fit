package com.flnk.pocketfit;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;


public class Profile extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    //spinner
    String[] levels = {"Sedentary: Little or no exercise", "Light: 1-3 times a week", "Moderate: 4-5 times a week", "Active: Daily or intense 3-4 times a week", "Very Active: Intense 6-7 times a week"};

//<item>Sedentary: Little or no exercise</item>
//<item>Light: 1-3 times a week</item>
//<item>Moderate: 4-5 times a week</item>
//<item>Active: Daily or intense 3-4 times a week</item>
//<item>Very Active: Intense 6-7 times a week</item>


        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            getSupportActionBar().hide();
            setContentView(R.layout.activity_profile);
            //Getting the instance of Spinner and applying OnItemSelectedListener on it
            Spinner spin = (Spinner) findViewById(R.id.spinner1);
            spin.setOnItemSelectedListener(this);

            //Creating the ArrayAdapter instance having the country list
            ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_item,levels);
            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            //Setting the ArrayAdapter data on the Spinner
            spin.setAdapter(aa);

        }

        //Performing action onItemSelected and onNothing selected
        @Override
        public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long id) {
            Toast.makeText(getApplicationContext(),levels[position] , Toast.LENGTH_LONG).show();
        }
        @Override
        public void onNothingSelected(AdapterView<?> arg0) {
            // TODO Auto-generated method stub
        }
    }



